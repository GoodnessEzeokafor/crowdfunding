require('chai')
    .use(require('chai-as-promised'))
    .should()


const Project = artifacts.require("./Project.sol")

contract("Project Smart Contract Unit Testng", ([deployer, creator, user]) => {
    before(async()=>{
        this.contract = await Project.deployed()
    })
    it("checks if the contract deployed successfully", async()=>{
        const address = this.contract.address
        assert.notEqual(address, 0x0)
        assert.notEqual(address, null)
        assert.notEqual(address, '')
        assert.notEqual(address, undefined)
        
    })
    it("PROJECT CONTRACT:- has dapp_name", async()=>{
        const dapp_name = await this.contract.dapp_name()
        assert.equal(dapp_name,"FundProj")
    })

    it("PROJECT CONTRACT:- has project count",async() => {
        const projectCount = await this.contract.projectCount()
        assert.equal(projectCount,0)
    })
    it("PROJECT CONTRACT:- has contribution count",async() => {
        const contributionCount = await this.contract.contributionCount()
        assert.equal(contributionCount,0)
    })

    it("PROJECT CONTRACT:- create project is working", async() => {
        var deadline = new Date('January 1, 2220 03:24:00');
        deadline=deadline.getTime()

        const newProject = await this.contract.createProject(
            "First Project",
            "First Project Description",
            "slide_hash_1",
            "slide_hash_2",
            "www.video.com",
            web3.utils.toWei('0.3','Ether'),
            deadline,
            {'from':creator}
        )
        const projectCount = await this.contract.projectCount()
        const result = newProject.logs[0].args
        assert.equal(result['id'].toString(),projectCount)
        assert.equal(result['creator'].toString(),creator)
        assert.equal(result['name'].toString(), "First Project")
        assert.equal(result['description'].toString(), "First Project Description")
        assert.equal(result['slide'].toString(), "slide_hash_1")
        assert.equal(result['video_url'].toString(), "www.video.com")
        assert.equal(result['goal'].toString(), web3.utils.toWei('0.3','Ether'))
        assert.equal(result['deadline'].toString(),deadline)
    })
    
    it("PROJECT CONTRACT:- returns project arrays",async() => {
        const projects = await this.contract.projects(1)
    })


    it("PROJECT CONTRACT:- current State ",async() => {
        const contract_state = await this.contract.state()
        console.log("State:",contract_state.toString())
        assert.equal(contract_state.toString(),0) // at index 0
    })

    it("PROJECT CONTRACT:- check if the contributionCount is working at initialization", async() => {
        const contributionCount = await this.contract.contributionCount()
        assert.equal(contributionCount.toString(), 0)
    })


    // /* SECTION II */
    it("PROJECT CONTRACT:- checks if the contribute function is working", async() =>{

        var contribute_result = await this.contract.contribute(1,{'from':user,'value':web3.utils.toWei('0.1','Ether')})
        // console.log(contribute_result)
        var project = await this.contract.get_single_project(1)
        // console.log("Project:", project)
        var current_state = await this.contract.state()
        const contributionCount = await this.contract.contributionCount()
        contribute_result = contribute_result.logs[0].args
        assert.equal(project['4'].toString(), web3.utils.toWei('0.1','Ether'))
        assert.equal(contributionCount.toString(), 1)
        // assert.equal(current_state.toString(),1)  // state should still be at Fundraising Since the Goal hasn't been met
        // should fails
        await this.contract.contribute({'from':creator}).should.be.rejected
        await this.contract.contribute({'from':creator, 'value':web3.utils.toWei('0','Ether')}).should.be.rejected
    })

    it("PROJECT CONTRACT:- checking if checkIfFundingCompleteOrExpired is working", async() => {
        let oldcreatorBalance
        var project = await this.contract.get_single_project(1)

        oldcreatorBalance =await web3.eth.getBalance(project['6'])
        oldcreatorBalance =new web3.utils.BN(oldcreatorBalance)


        var contribute_result = await this.contract.contribute(1,{'from':user,'value':web3.utils.toWei('0.3','Ether')})
        var current_state = await this.contract.state()
        // var result = await this.contract.checkIfFundingCompleteOrExpired(1)
        assert.equal(current_state.toString(),2)  // state should still be at Successful Since the Goal has been met




        //     // // check the creator recieve funds
            let newcreatorBalance;
            newcreatorBalance =await web3.eth.getBalance(project['6'])
            newcreatorBalance =new web3.utils.BN(newcreatorBalance)
    
        //     // let price
            price = web3.utils.toWei('0.4','Ether')
            price = new web3.utils.BN(price)
            
    
            const expectedBalance = oldcreatorBalance.add(price)
            assert.equal(newcreatorBalance.toString(), expectedBalance.toString())
        
    })

    // it("PROJECT CONTRACT:- check if the contributionCount is working", async() => {
    //     const contributionCount = await this.contract.contributionCount()
    //     assert.equal(contributionCount.toString(), 2)
    // })
})