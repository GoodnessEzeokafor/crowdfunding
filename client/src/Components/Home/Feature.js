import React, { Component } from 'react';

export default class Feature extends Component {
    render() {
        return (
            <section id="feature" style={{marginTop : "80px"}}>
            <div className="container">
                <div className="center fadeInDown">
                    <h2>Features</h2>
                    <p className="lead">
                        This platform uses the blockchain and cryptotechnology to enable you get fundings for your ideas.</p>
                </div>
    
                <div className="row">
                    <div className="features">
                        <div className="col-md-3 col-sm-4 fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                            <div className="feature-wrap">
                                <div className="icon">
                                    <i className="fa fa-rocket"></i>
                                </div>
                                <h2>Fast</h2>
                                <p>Transactions and fundings are made instantly</p>
                            </div>
                        </div>
                        {/* <!--/.col-md-3--> */}
                        <div className="col-md-3 col-sm-4 fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                            <div className="feature-wrap">
                                <div className="icon">
                                    <i className="fa fa-check"></i>
                                </div>
                                <h2>Easy</h2>
                                <p>All it takes is a push of a button</p>
                            </div>
                        </div>
                        {/* <!--/.col-md-3--> */}
                        <div className="col-md-3 col-sm-4 fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                            <div className="feature-wrap">
                                <div className="icon">
                                    <i className="fa fa-bullhorn"></i>
                                </div>
                                <h2>Cheap</h2>
                                <p>No middle men increasing charges and costs of transfer</p>
                            </div>
                        </div>
                        {/* <!--/.col-md-3--> */}
                        <div className="col-md-3 col-sm-4 fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                            <div className="feature-wrap">
                                <div className="icon">
                                    <i className="fa fa-arrows"></i>
                                </div>
                                <h2>Secure</h2>
                                <p>Your details are kept anonymous and safe</p>
                            </div>
                        </div>
                        {/* <!--/.col-md-3--> */}
                    </div>
                    {/* <!--/.services--> */}
                </div>
                {/* <!--/.row--> */}
            </div>
            {/* <!--/.container--> */}
        </section>
    
        );
    }
}