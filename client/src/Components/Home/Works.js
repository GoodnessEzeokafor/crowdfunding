// eslint-disable-next-line
import React, { Component } from 'react';

export default class Works extends Component {
    render() {
        return (
            <section id="recent-works" style={{marginTop : "80px"}}>
            <div className="container">
                <div className="center fadeInDown">
                    <h2>Recent Works</h2>
                    <p className="lead">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br /> et dolore magna aliqua. Ut enim ad minim veniam</p>
                </div>
    
                <div className="row">
                    <div className="col-xs-12 col-sm-6 col-md-4 single-work">
                        <div className="recent-work-wrap">
                            <img className="img-responsive" src="/images/portfolio/item-1.png" alt="" />
                            <div className="overlay">
                                <div className="recent-work-inner">
                                    <a className="preview" href="/images/portfolio/item-1.png" rel="prettyPhoto">
                                        <i className="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div className="col-xs-12 col-sm-6 col-md-4 single-work">
                        <div className="recent-work-wrap">
                            <img className="img-responsive" src="/images/portfolio/item-2.png" alt="" />
                            <div className="overlay">
                                <div className="recent-work-inner">
                                    <a className="preview" href="/images/portfolio/item-2.png" rel="prettyPhoto">
                                        <i className="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div className="col-xs-12 col-sm-6 col-md-4 single-work">
                        <div className="recent-work-wrap">
                            <img className="img-responsive" src="images/portfolio/item-3.png" alt="" />
                            <div className="overlay">
                                <div className="recent-work-inner">
                                    <a className="preview" href="/images/portfolio/item-3.png" rel="prettyPhoto"><i className="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div className="col-xs-12 col-sm-6 col-md-4 single-work">
                        <div className="recent-work-wrap">
                            <img className="img-responsive" src="/images/portfolio/item-4.png" alt="" />
                            <div className="overlay">
                                <div className="recent-work-inner">
                                    <a className="preview" href="/images/portfolio/item-4.png" rel="prettyPhoto"><i className="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div className="col-xs-12 col-sm-6 col-md-4 single-work">
                        <div className="recent-work-wrap">
                            <img className="img-responsive" src="/images/portfolio/item-5.png" alt="" />
                            <div className="overlay">
                                <div className="recent-work-inner">
                                    <a className="preview" href="/images/portfolio/item-5.png" rel="prettyPhoto"><i className="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div className="col-xs-12 col-sm-6 col-md-4 single-work">
                        <div className="recent-work-wrap">
                            <img className="img-responsive" src="/images/portfolio/item-6.png" alt="" />
                            <div className="overlay">
                                <div className="recent-work-inner">
                                    <a className="preview" href="/images/portfolio/item-6.png" rel="prettyPhoto"><i className="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* <!--/.row--> */}
                <div className="clearfix text-center">
                    <br />
                    <br />
                    {/* // eslint-disable-next-line */}
                    <a  className="btn btn-primary">Show More</a>
                </div>
            </div>
            {/* <!--/.container--> */}
        </section>
    
        );
    }
}