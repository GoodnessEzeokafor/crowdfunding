import React, { Component } from 'react';

export default class Slider extends Component {
    render() {
        return (
            <section id="main-slider" className="no-margin">
            <div className="carousel slide">
                <ol className="carousel-indicators">
                    <li data-target="#main-slider" data-slide-to="0" className="active"></li>
                    <li data-target="#main-slider" data-slide-to="1"></li>
                    <li data-target="#main-slider" data-slide-to="2"></li>
                </ol>
                <div className="carousel-inner">
    
                    <div className="item active" style={{backgroundImage: "url(https://kuulpeeps.com/wp-content/uploads/2017/11/Crowdfunding.jpeg)" }}>
                        <div className="container">
                            <div className="row">
                                <div className="col-md-7">
                                    <div className="carousel-content">
                                        <h1 className="animation animated-item-1" >Come CrownFund your idea</h1>
                                        <div className="animation animated-item-2">
                                            Get your idea on track today with the right funding
                                        </div>
                                        <a className="btn-slide animation animated-item-3" href="#">Learn More</a>
                                        <a className="btn-slide white animation animated-item-3" href="#">Get Started</a>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
                    {/* <!--/.item--> */}
    
                    <div className="item" style={{ backgroundImage: "url(/images/slider/bg2.jpg)" }}>
                        <div className="container">
                            <div className="row">
                                <div className="col-md-7">
                                    <div className="carousel-content">
                                        <h1 className="animation animated-item-1">Crowd Funding</h1>
                                        <div className="animation animated-item-2">
                                            One idea is all it takes
                                        </div>
                                        <a className="btn-slide white animation animated-item-3" href="#">Learn More</a>
                                        <a className="btn-slide animation animated-item-3" href="#">Get Started</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!--/.item--> */}
                </div>
                {/* <!--/.carousel-inner--> */}
            </div>
            {/* <!--/.carousel--> */}
            <a className="prev hidden-xs hidden-sm" href="#main-slider" data-slide="prev">
                <i className="fa fa-chevron-left"></i>
            </a>
            <a className="next hidden-xs hidden-sm" href="#main-slider" data-slide="next">
                <i className="fa fa-chevron-right"></i>
            </a>
        </section>
    
        );
    }
}