import React, { Component } from 'react';

export default class Partners extends Component {
    render() {
        return (
            <section id="partner" style={{marginTop : "80px"}}>
            <div className="container">
                <div className="center fadeInDown">
                    <h2>Our Partners</h2>
                    <p className="lead">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br /> et dolore magna aliqua. Ut enim ad minim veniam</p>
                </div>
    
                <div className="partners">
                    <ul>
                        <li>
                            <a href="#"><img className="img-responsive fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" src="/images/partners/brand-1.png" /></a>
                        </li>
                        <li>
                            <a href="#"><img className="img-responsive fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" src="/images/partners/brand-2.png" /></a>
                        </li>
                        <li>
                            <a href="#"><img className="img-responsive fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms" src="/images/partners/brand-3.png" /></a>
                        </li>
                        <li>
                            <a href="#"><img className="img-responsive fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" src="/images/partners/brand-4.png" /></a>
                        </li>
                        <li>
                            <a href="#"><img className="img-responsive fadeInDown" data-wow-duration="1000ms" data-wow-delay="1500ms" src="/images/partners/brand-5.png" /></a>
                        </li>
                    </ul>
                </div>
            </div>
            {/* <!--/.container--> */}
        </section>
    
        );
    }
}