import React, { Component } from 'react';

export default class Testimonial extends Component {
    render() {
        return (
            
<section id="testimonial" style={{marginTop : "80px"}}>
<div className="container">
    <div className="center fadeInDown">
        <h2>Testimonials</h2>
        <p className="lead">
            Testimonies from people who have used this platform <br /></p>
    </div>
    <div className="testimonial-slider owl-carousel">
        <div className="single-slide">
            <div className="slide-img">
                <img src="/images/client1.jpg" alt="" />
            </div>
            <div className="content">
                <img src="/images/review.png" alt="" />
                <p>
                    If you are looking at blank cassettes on the web, you may be very confused at the 
                    difference in price.</p>
                <h4>- Emmanuel Nduka</h4>
            </div>
        </div>
        <div className="single-slide">
            <div className="slide-img">
                <img src="/images/client2.jpg" alt="" />
            </div>
            <div className="content">
                <img src="/images/review.png" alt="" />
                <p>If you are looking at blank cassettes on the web, you may be very confused at the difference in price.</p>
                <h4>- Goodness EzeOkafor</h4>
            </div>
        </div>
        <div className="single-slide">
            <div className="slide-img">
                <img src="/images/client3.jpg" alt="" />
            </div>
            <div className="content">
                <img src="/images/review.png" alt="" />
                <p>If you are looking at blank cassettes on the web, you may be very confused at the difference in price.</p>
                <h4>- Another person</h4>
            </div>
        </div>
        <div className="single-slide">
            <div className="slide-img">
                <img src="/images/client2.jpg" alt="" />
            </div>
            <div className="content">
                <img src="/images/review.png" alt="" />
                <p>If you are looking at blank cassettes on the web, you may be very confused at the difference in price.</p>
                <h4>- Lionel Messi</h4>
            </div>
        </div>
        <div className="single-slide">
            <div className="slide-img">
                <img src="/images/client1.jpg" alt="" />
            </div>
            <div className="content">
                <img src="/images/review.png" alt="" />
                <p>If you are looking at blank cassettes on the web, you may be very confused at the difference in price.</p>
                <h4>- Daniel James</h4>
            </div>
        </div>
        <div className="single-slide">
            <div className="slide-img">
                <img src="/images/client3.jpg" alt="" />
            </div>
            <div className="content">
                <img src="/images/review.png" alt="" />
                <p>If you are looking at blank cassettes on the web, you may be very confused at the difference in price.</p>
                <h4>- Paul Pogba</h4>
            </div>
        </div>
    </div>
</div>
</section>

        );
    }
}
