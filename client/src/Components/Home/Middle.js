import React, { Component } from 'react';

export default class Middle extends Component {
    render() {
        return (
            <section id="middle" className="skill-area" style={{ backgroundImage: "url(/images/skill-bg.png)",marginTop : "80px"}}>       
             <div className="container">
            <div className="row">
                <div className="col-sm-12 fadeInDown">
                    <div className="skill">
                        <h2>Our Skills</h2>
                        <p>
                            All users on CrowdFunding will know that there are millions of people out there. Every <br /> day besides so many people joining this community to fund and be funded.</p>
                    </div>
                </div>
                {/* <!--/.col-sm-6--> */}

                <div className="col-sm-6">
                    <div className="progress-wrap">
                        <h3>Anonimity</h3>
                        <div className="progress">
                            <div 
                            className="progress-bar  color1" 
                            role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="10" style={{ width: "100%"}}>
                                <span className="bar-width">100%</span>
                            </div>

                        </div>
                    </div>

                    <div className="progress-wrap">
                        <h3>Speed</h3>
                        <div className="progress">
                            <div 
                                className="progress-bar color2" 
                                role="progressbar" 
                                aria-valuenow="20" 
                                aria-valuemin="0" 
                                aria-valuemax="100" 
                                style={{width: "95%" }}>
                                <span className="bar-width">95%</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-sm-6">
                    <div className="progress-wrap">
                        <h3>Transparency</h3>
                        <div className="progress">
                            <div 
                            className="progress-bar color3" 
                            role="progressbar" 
                            aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style={{ width: "80%"}}>
                                <span className="bar-width">80%</span>
                            </div>
                        </div>
                    </div>

                    <div className="progress-wrap">
                        <h3>Quality</h3>
                        <div className="progress">
                            <div 
                                className="progress-bar color4" 
                                role="progressbar" 
                                aria-valuenow="80" 
                                aria-valuemin="0" 
                                aria-valuemax="100" 
                                style={{ width: "90%" }}>
                                <span className="bar-width">90%</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            {/* <!--/.row--> */}
        </div>
        {/* <!--/.container--> */}
    </section>

        );
    }
}