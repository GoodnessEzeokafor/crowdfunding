import React, { Component } from 'react';

export default class Services extends Component {
    render() {
        return (
            <section id="services" className="service-item" style={{marginTop : "80px"}}>
            <div className="container">
                <div className="center fadeInDown">
                    <h2>Our Service</h2>
                    <p className="lead">
                       This is a BlockChain CrowdFunding Application <br /> It enables you get funded via the blockchain</p>
                </div>
    
                <div className="row">
                    <div className="col-sm-6 col-md-4">
                        <div className="media services-wrap fadeInDown">
                            <div className="pull-left">
                                <img className="img-responsive" alt="" src="/images/services/ux.svg" />
                            </div>
                            <div className="media-body">
                                <h3 className="media-heading">Crypto</h3>
                                <p>Enables transfer of funds via cryptocurrencies</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-4">
                        <div className="media services-wrap fadeInDown">
                            <div className="pull-left">
                                <img className="img-responsive" alt="" src="/images/services/web.svg" />
                            </div>
                            <div className="media-body">
                                <h3 className="media-heading">Web Design</h3>
                                <p>Hydroderm is the highly desired anti-aging cream on</p>
                            </div>
                        </div>
                    </div>
    
                    <div className="col-sm-6 col-md-4">
                        <div className="media services-wrap fadeInDown">
                            <div className="pull-left">
                                <img className="img-responsive" alt="" src="/images/services/motion.svg" />
                            </div>
                            <div className="media-body">
                                <h3 className="media-heading">Funding</h3>
                                <p>Enables you acquire funding instantly</p>
                            </div>
                        </div>
                    </div>
    
                    <div className="col-sm-6 col-md-4">
                        <div className="media services-wrap fadeInDown">
                            <div className="pull-left">
                                <img className="img-responsive" alt="" src="/images/services/mobile-ui.svg" />
                            </div>
                            <div className="media-body">
                                <h3 className="media-heading">Anonimity</h3>
                                <p>Keeps your identity completely invincible</p>
                            </div>
                        </div>
                    </div>
    
                    <div className="col-sm-6 col-md-4">
                        <div className="media services-wrap fadeInDown">
                            <div className="pull-left">
                                <img alt="" className="img-responsive" src="/images/services/web-app.svg" />
                            </div>
                            <div className="media-body">
                                <h3 className="media-heading">Speed</h3>
                                <p>Transactions are fast and instantaneous</p>
                            </div>
                        </div>
                    </div>
    
                    <div className="col-sm-6 col-md-4">
                        <div className="media services-wrap fadeInDown">
                            <div className="pull-left">
                                <img alt="" className="img-responsive" src="/images/services/mobile-ui.svg" />
                            </div>
                            <div className="media-body">
                                <h3 className="media-heading">Location</h3>
                                <p>Transactions can be made from anywhere world wide</p>
                            </div>
                        </div>
                    </div>
    
                </div>
                {/* <!--/.row--> */}
            </div>
            {/* <!--/.container--> */}
        </section>
    
        );
    }
}