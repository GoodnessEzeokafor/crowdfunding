import React, { Component } from 'react';

export default class ContentModal extends Component {
    constructor(props) {
        super(props);
        
    }
    render() {
        const showHideClassName = this.props.show ? "modal  and carousel slide display-block" : "modal display-none";

        return (
            <div
            className={showHideClassName} 
              // class="modal fade" 
              tabIndex="-1" role="dialog">
                  <div className="modal-dialog" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <button onClick={this.props.handleClose} type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 className="modal-title text-center">PROJECT NAME: {this.props.single_project['1']}</h4>
                </div>
                <div className="modal-body">
            <p>{this.props.single_project['2']}</p>
            <p>Total Amount Raised {this.props.single_project['4']/1000000000000000000} ETH</p>
            <p>Deadline {new Date(parseInt(this.props.single_project['3'],10)).toString()}</p>
            <p>Campaigned Closed on {this.props.single_project['5']} </p>
            <p>Project Creator {this.props.single_project['6']}</p>
            <p>Click here to watch video <a href={`${this.props.single_project['9']}`}> here</a></p>

            <p>Project Goals:{this.props.single_project['10']/1000000000000000000 }
                    {/* {window.web3.utils.fromWei(this.props.single_project['10'],'Ether')} */}
                    ETH</p>
                </div>  
                <div className="modal-footer">
                <button 
                    type="button" 
                    id={this.props.single_project['0']}
                    className="btn btn-info"
                    onClick= {async (event) => {
                        console.log("Hello World")
                        const event_ = parseInt(event.target.id,10)
                        await this.props.ProjectDapp.methods.contribute(event_)
                        .send({from: this.props.account, 
                                value:2*1000000000000000000});
                        console.log("EVENT ID:",event_)
                        event.persist();
                        event.preventDefault()
                        // alert("Contributed successfully")
                        // window.location.reload(true)

                    }}
                    >Contribute</button>              
                  <button 
                    onClick={this.props.handleClose} 
                    data-dismiss="modal" 
                    type="button" 
                    className="btn btn-primary">Close</button>
                </div>
              </div>
            </div> 
            </div>
            
        );
    }
}