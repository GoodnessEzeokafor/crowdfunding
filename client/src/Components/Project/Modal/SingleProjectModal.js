
import React, { Component } from 'react';

export default class Modal extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const showHideClassName = this.props.show ? "modal  and carousel slide display-block" : "modal display-none";
        return (
            <div
            className={showHideClassName} 
              // class="modal fade" 
              tabIndex="-1" role="dialog">


 <div class="" id="lightbox">
 <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-body">
       <ol class="carousel-indicators">
         <li data-target="#lightbox" data-slide-to="0" class="active"></li>
         <li data-target="#lightbox" data-slide-to="1"></li>
         <li data-target="#lightbox" data-slide-to="2"></li>
       </ol>
       <div class="carousel-inner">
         <div class="item active">
           <img src="http://placehold.it/900x500/777/" alt="First slide" />
         </div>
         <div class="item">
           <img src="http://placehold.it/900x500/666/" alt="Second slide" />
         </div>
         <div class="item">
           <img src="http://placehold.it/900x500/555/" alt="Third slide" />
           <div class="carousel-caption"><p>even with captions...</p></div>
         </div>
       </div>
       <a class="left carousel-control" href="#lightbox" role="button" data-slide="prev">
         <span class="glyphicon glyphicon-chevron-left"></span>
       </a>
       <a class="right carousel-control" href="#lightbox" role="button" data-slide="next">
         <span class="glyphicon glyphicon-chevron-right"></span>
       </a>
     </div>
     <div className="modal-footer">
              <button 
                onClick={this.props.handleClose} 
                data-dismiss="modal" 
                type="button" 
                className="btn btn-primary">Close</button>
            </div>
   </div>
 </div>
 </div>

      </div>
        );
    }
}





