import React, { Component } from 'react';
import "./File.css"
// import {Editor, EditorState} from 'draft-js';

// ipfs
const ipfsClient = require('ipfs-http-client')
const ipfs = ipfsClient({host:'ipfs.infura.io',port:5001,'protocol':'https'})



export default class Form extends Component {
    constructor(props){
        super(props)
        this.state = {
            loading:false,
            slide1_buffer:null,
            slide2_buffer:null,
            slide_1_hash:"",
            slide_2_hash:""
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    async  handleSubmit(event){
        console.log("############Handling Form############")
        console.log("Project Name: ",this.name.value)
        console.log("Project Description:", this.description.value)
        console.log("Slide 1:", this.slide_1.value)
        console.log("Slide 2:",this.slide_2.value)
        console.log("Video Url:",this.video_url.value)
        console.log("Deadline:",this.deadline.value)
        console.log("Goal:", this.goal.value)


        try {
            console.log("Events")
            this.setState({loading:true})

          // Write to the Blockchains
          this.props.ProjectDapp.methods.createProject(
            this.name.value, // name of project
            this.description.value, // description of project
            this.state.slide_1_hash, // slide 1 hash
            this.state.slide_2_hash, // slide 2 hash
            this.video_url.value, // video url
            window.web3.utils.toWei(this.goal.value.toString(),'Ether'), // goal in ether   
            new Date(this.deadline.value).getTime() // deadline
        )
        .send({from:this.props.account})
        .once('receipt', (receipt) => {
            console.log(receipt);
            this.setState({loading:false})

        })


        } catch (error){
            console.log(error)
        }
        console.log("############END Handling Form############")
        event.preventDefault();
        // event.persist();
        event.stopPropagation();

    }

        //Capture File 1
        captureFile1 =(event) => {
            event.preventDefault()
        const file = this.slide_1.files[0]
        const reader = new window.FileReader()
        reader.readAsArrayBuffer(file)
        reader.onloadend = () =>{
            console.log("buffer", this.state.slide1_buffer)
            console.log("buffer", Buffer(reader.result))
            this.setState({
                slide1_buffer:Buffer(reader.result)
            })


        // FOR SLIDE 1
        ipfs.add(this.state.slide1_buffer,(error, result)=>{
            // do stuff here
            console.log("Ipfs Result",result)
            // return result['0'].hash
            result.forEach(async (file) => {
                console.log("successfully stored", file.hash)
                this.setState({
                    slide_1_hash:file.hash
                })
            });
            if(error){
                console.error("Error>>>", error)
                return
            }
        })
        // END
        }
    }

    // Capture File 2

    captureFile2 =(event) => {
    event.preventDefault()
    const file = this.slide_2.files[0]
    const reader = new window.FileReader()
    reader.readAsArrayBuffer(file)
    reader.onloadend = () =>{
        console.log("buffer", this.state.slide2_buffer)
        console.log("buffer", Buffer(reader.result))
        this.setState({
            slide2_buffer:Buffer(reader.result)
        })


    // FOR SLIDE 2
    ipfs.add(this.state.slide2_buffer,(error, result)=>{
        // do stuff here
        console.log("Ipfs Result",result)
        // return result['0'].hash
        result.forEach(async (file) => {
            console.log("successfully stored", file.hash)
            this.setState({
                slide_2_hash:file.hash
            })
                
        });
        if(error){
            console.error("Error>>>", error)
            return
        }
    })
    }
}

    render() {
        return (
            <div>
                <div className="page-title" style={{backgroundImage: "url(/images/page-title.png)"}}>
                    <h1>Start a project </h1>
                </div>

                {/* Loader */}
                {this.state.loading ? 
                    <div class="cssload-loader-inner">
                    <div class="cssload-cssload-loader-line-wrap-wrap">
                        <div class="cssload-loader-line-wrap"></div>
                    </div>
                    <div class="cssload-cssload-loader-line-wrap-wrap">
                        <div class="cssload-loader-line-wrap"></div>
                    </div>
                    <div class="cssload-cssload-loader-line-wrap-wrap">
                        <div class="cssload-loader-line-wrap"></div>
                    </div>
                    <div class="cssload-cssload-loader-line-wrap-wrap">
                        <div class="cssload-loader-line-wrap"></div>
                    </div>
                    <div class="cssload-cssload-loader-line-wrap-wrap">
                        <div class="cssload-loader-line-wrap"></div>
                    </div>
                    </div>                
                :
                <section id="contact-page">
                <div className="container">
                    <div className="large-title text-center">        
                        <h2>Drop Your Message</h2>
                        <p>
                            All users on MySpace will know that there are millions of people out there. 
                            Every day <br /> besides so many people joining this community.</p>
                    </div> 
                    <div className="row contact-wrap"> 
                        <div className="status alert alert-success" style={{ display: "none"}}></div>
                        <form onSubmit={this.handleSubmit}>
                            <div className="col-sm-5 col-sm-offset-1">
                                {/* PROJECT NAME */}
                                <div className="form-group">
                                    <label>Project Name *</label>
                                    <input 
                                        type="text" 
                                        name="name" 
                                        className="form-control" 
                                        required="required" 
                                        ref = {(input)=> this.name = input}
                                        />
                                </div>
                                {/* Slide 1 */}
                                <div className="form-group">
                                    <label class="btn-bs-file btn btn-lg btn-danger">
                                        Browse
                                        <input 
                                            type="file"
                                            onChange={this.captureFile1}
                                            ref = {(input)=> this.slide_1 = input}
                                         />
                                    </label>
                                    <p><b>Slide 1</b></p>
                                </div>
        
                                {/* Slide 2 */}
                                <div className="form-group">
                                    <label class="btn-bs-file btn btn-lg btn-danger">
                                        Browse
                                        <input 
                                            type="file"
                                            onChange={this.captureFile2}
                                            ref = {(input)=> this.slide_2 = input} 
                                            />
                                    </label>
                                    <p><b>Slide 2</b></p>
                                </div>
                                {/* VIDEO URL */}
                                <div className="form-group">
                                <label htmlFor="basic-url">Video URL</label>
                                <div className="input-group">
                                <span className="input-group-addon" id="basic-addon3">https://example.com/users/</span>
                                <input 
                                    type="text" 
                                    className="form-control" 
                                    id="basic-url" 
                                    aria-describedby="basic-addon3"
                                    placeholder="e.g https://example.com/users/"
                                    ref = {(input)=> this.video_url = input} 
                                    />
                                </div>
                                </div>
                                {/* GOAL */}
                                <div className="form-group">
                                    <label>Amount To Be Raised(*Goal)</label>
                                    <input 
                                        type="text" 
                                        className="form-control"
                                        placeholder="In Ether e.g 5 , 0.5, 2.5"
                                        required
                                        ref = {(input)=> this.goal = input}
                                        />
                                </div>                        
                            </div>
                            {/* PROJECT DEADLINE */}
                            <div className="col-sm-5">
                                <div className="form-group">
                                    <label>Project Deadline *</label>
                                    <input 
                                        type="date" 
                                        name="subject" 
                                        className="form-control"
                                        ref = {(input)=> this.deadline = input} 
                                        required="required" />
                                </div>
                                {/* PROJECT DESCRIPTION */}
                                <div className="form-group">
                                    <label>Project Description *</label>
                                    {/* <MarkdownTextarea />
                                     */}
                                        {/* <Editor editorState={this.state.editorState} onChange={this.onChange} /> */}
                                    <textarea 
                                        name="message" 
                                        id="message" 
                                        required="required" 
                                        className="form-control" 
                                        rows="8" 
                                        ref = {(input)=> this.description = input}
                                        ></textarea>
                      </div>                        
                                <div className="form-group">
                                    <button type="submit" name="submit" className="btn btn-primary btn-lg" required="required">Submit Project</button>
                                </div>
                            </div>
                        </form> 
                    </div>
                    {/* <!--/.row--> */}
                </div>
                {/* <!--/.container--> */}
            </section>
        }
                {/* END LOADER */}
    {/* <!--/#contact-page--> */}
            </div>
        );
    }
}