import React, { Component } from "react";
// import { Route, Switch } from 'react-router-dom'
// import {
//         Route,
//         Link,
//         // Switch,
//         BrowserRouter as Router } from 'react-router-dom'

import "./Project/File.css";
import "./modal.css";
import ImageModal from "./Project/Modal/SingleProjectModal";
import ContentModal from "./Project/Modal/ContentModal";

export default class Projects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      single_project: "",
      // addModalShow: false,
      show1: false,
      show2:false,
      loader: false
    };
  }
  async getSingleProject(id) {
    const get_single_project = await this.props.ProjectDapp.methods
      .get_single_project(id)
      .call();
    console.log("Writing To The Blockchain");
    console.log(get_single_project);
    return get_single_project;
  }
  showModal = () => {
    this.setState({ show1: true });
  };
  showModal2 = () => {
    this.setState({show2 : true})
  }
  hideModal = () => {
    this.setState({ show1: false });
  };
  hideModal2 = () => {
    this.setState({ show2: false });
  };

  render() {
    return (
      <div>
        <div
          className="page-title"
          style={{ backgroundImage: "url(images/page-title.png)" }}
        >
          <h1>Project</h1>
        </div>

        <section id="blog" style={{ marginTop: "80px" }}>
          <div className="blog container">
            <div className="row">
              <div className="col-md-8">
                {/* // <div class="cssload-loader-inner">
                        //     <div class="cssload-cssload-loader-line-wrap-wrap">
                        //         <div class="cssload-loader-line-wrap"></div>
                        //     </div>
                        //     <div class="cssload-cssload-loader-line-wrap-wrap">
                        //         <div class="cssload-loader-line-wrap"></div>
                        //     </div>
                        //     <div class="cssload-cssload-loader-line-wrap-wrap">
                        //         <div class="cssload-loader-line-wrap"></div>
                        //     </div>
                        //     <div class="cssload-cssload-loader-line-wrap-wrap">
                        //         <div class="cssload-loader-line-wrap"></div>
                        //     </div>
                        //     <div class="cssload-cssload-loader-line-wrap-wrap">
                        //         <div class="cssload-loader-line-wrap"></div>
                        //     </div>
                        // </div> */}
                {this.props.projects.map((project, key) => {
                  return (
                    <div className="blog-item" key={key}>

                      {/* Goody thi will handle the opening and closing of the modal, this modal declaration is at the bottom of the code */}
                      <ImageModal
                        show={this.state.show1}
                        handleClose={this.hideModal}
                        single_project={this.state.single_project}
                      >
                        <p>This is a modal, can add content here</p>
                      </ImageModal>
                      <ContentModal
                        show={this.state.show2}
                        handleClose={this.hideModal2}
                        single_project={this.state.single_project}
                        ProjectDapp = {this.props.ProjectDapp}
                        account={this.props.account}
                      />
                      <a href="#">
                        <img
                          className="img-responsive img-blog"
                          src={`https://ipfs.io/ipfs/${project.slide}/`}
                          width="100%"
                          alt=""
                        />
                      </a>
                      <div class="blog-content">
                        <a href="#" class="blog_cat">
                          {project.name}
                        </a>
                        <h2>
                          <a href="blog-item.html">{project.name}</a>
                        </h2>
                        <h3>{project.description}</h3>
                        {/* BUTTON 1 */}
                        <button
                          className="btn btn-primary"
                          id={project.id}
                          data-target="#exampleModal"
                          //  to={`/project/${project.id}`}
                          onClick={async(event) => {

                            this.showModal();
                            const id = parseInt(event.target.id, 10);
                            // console.log(id, typeof id);
                            const single_project = await this.getSingleProject(id);
                           console.log("Content:",single_project["name"])
                            this.setState({ single_project });
                            event.persist();
                          }
                          }
                        >
                          View Images
                          <i className="fa fa-long-arrow-right"></i>
                        </button>
                        {/* BUTTON 2 */}
                        <button
                          className="btn btn-primary"
                          id={project.id}
                          data-target="#exampleModal"
                          style={{marginLeft:"20px"}}
                          //  to={`/project/${project.id}`}
                          onClick={async(event) => {
                            this.showModal2();
                            const id = parseInt(event.target.id, 10);
                            // console.log(id, typeof id);
                            const single_project = await this.getSingleProject(id);
                           console.log("Content:",single_project["name"])
                            this.setState({ single_project });
                            event.persist();
                          }
                            // console.log("Hello WOrld")
                            // window.alert("Hello")
                            
                            // {this.showModal()}
                          }
                        >
                        Read More
                          <i className="fa fa-long-arrow-right"></i>
                        </button>
                        
                      </div>
                    </div>
                  );
                })}
                {/* <!--/.blog-item--> */}

                {/* <!--/.blog-item--> */}
                {/* <Modal
                  show={this.state.show}
                  handleClose={this.hideModal}
                  // onHide={addModalClose}
                /> */}
              </div>
              {/* <!--/.col-md-8--> */}

              <aside className="col-md-4">
                <div className="widget blog_gallery">
                  <h3>Our Gallery</h3>
                  <ul className="sidebar-gallery clearfix">
                    {this.props.projects.map((project, key) => {
                      return (
                        <li key={key}>
                          <a href="#">
                            <img
                              src={`https://ipfs.io/ipfs/${project.slide}/`}
                              alt=""
                            />
                          </a>
                        </li>
                      );
                    })}

                    {/* <li>
                                    <a href="#"><img src="images/sidebar-g-2.png" alt="" /></a>
                                </li>
                                <li>
                                    <a href="#"><img src="images/sidebar-g-3.png" alt="" /></a>
                                </li>
                                <li>
                                    <a href="#"><img src="images/sidebar-g-4.png" alt="" /></a>
                                </li>
                                <li>
                                    <a href="#"><img src="images/sidebar-g-5.png" alt="" /></a>
                                </li>
                                <li>
                                    <a href="#"><img src="images/sidebar-g-6.png" alt="" /></a>
                                </li> */}
                  </ul>
                </div>
                {/* <!--/.blog_gallery--> */}

                <div className="widget social_icon">
                  <a href="#" className="fa fa-facebook"></a>
                  <a href="#" className="fa fa-twitter"></a>
                  <a href="#" className="fa fa-linkedin"></a>
                  <a href="#" className="fa fa-pinterest"></a>
                  <a href="#" className="fa fa-github"></a>
                </div>
              </aside>
            </div>
            {/* <!--/.row--> */}
            <div className="row">
              <div className="col-md-12 text-center">
                <ul className="pagination pagination-lg">
                  <li>
                    <a href="#">
                      <i className="fa fa-long-arrow-left"></i>
                    </a>
                  </li>
                  <li className="active">
                    <a href="#">1</a>
                  </li>
                  <li>
                    <a href="#">2</a>
                  </li>
                  <li>
                    <a href="#">3</a>
                  </li>
                  <li>
                    <a href="#">4</a>
                  </li>
                  <li>
                    <a href="#">5</a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-long-arrow-right"></i>
                    </a>
                  </li>
                </ul>
                {/* <!--/.pagination--> */}
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

// // Handles the cpntent pf the modal
// const Modal =  ({ handleClose, show, children ,single_project}) => {
//   const showHideClassName = show ? "modal display-block" : "modal display-none";
//   // const new_project = await single_project
//   // console.log("Single Project:", new_project)
//   return (

//     <div
//         className={showHideClassName} 
//           // class="modal fade" 
//           tabIndex="-1" role="dialog">
//     <div class="modal-dialog" role="document">
//       <div class="modal-content">
//         <div class="modal-header">
//           <button onClick={handleClose} type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
//   <h4 class="modal-title"></h4>
//         </div>
//         <div class="modal-body">
//           <p>One fine body&hellip;</p>
//         </div>
//         <div class="modal-footer">
//           <button onClick={handleClose} data-dismiss="modal" type="button" class="btn btn-primary">Close</button>
//         </div>
//       </div>
//     </div>
//   </div>
//   );
// };
