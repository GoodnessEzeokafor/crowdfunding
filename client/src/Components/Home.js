import React, { Component } from 'react';
import Slider from "./Home/Slider"
import Feature from "./Home/Feature"
import Works from "./Home/Works"
import Services from "./Home/Services"
import Middle from "./Home/Middle"
import Content from "./Home/Content"
import Testemonial from "./Home/Testimonial"
import Partners from "./Home/Partners"

export default class Home extends Component {
   
    render() {    
        return (
            <div>
                <Slider />
                <Feature />
                <Works />
                <Services />
                <Middle />
                <Content />
                <Testemonial />
                <Partners />
            </div>        
        );
    }
}