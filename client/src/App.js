// React Router
import { Route, Link, Switch, BrowserRouter as Router } from "react-router-dom";
import React, { Component } from "react";

// Custom Components
import Home from "./Components/Home";
import Footer from "./Components/Home/Footer";
import Projects from "./Components/Projects"
import Form from "./Components/Project/Form"
// Web 3
import Web3 from "web3";
import ProjectAbi from "./abis/Project.json";

class App extends Component {
  async componentWillMount() {
    await this.loadWeb3();
    await this.loadBlockchainData();
  }

  async loadWeb3() {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum);
      await window.ethereum.enable();
    } else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider);
    } else {
      window.alert(
        "Non-Ethereum browser detected. You should consider trying MetaMask!"
      );
    }
  }
  async loadBlockchainData() {
    // console.log(SocialNetwork)
    const web3 = window.web3;
    window.web3 = new Web3(window.ethereum);
    
    //     // // load accounts
        const accounts = await web3.eth.getAccounts() // returns all the account in our wallet 
        this.setState({account:accounts[0]})
        // console.log(accounts)
    
    //     // // detects the network dynamically 
        const networkId = await web3.eth.net.getId()
    
    //     // // get network data
        const ProjectnetworkData= ProjectAbi.networks[networkId]
        if(ProjectnetworkData){
        //   const CrowdFundingDapp = new web3.eth.Contract(CrowdFunding.abi, CrowdnetworkData.address) // loads the smart contract
          const ProjectDapp = new web3.eth.Contract(ProjectAbi.abi, ProjectnetworkData.address) // loads the smart contract    
          console.log(ProjectDapp)
    
    
          const projectCount = await ProjectDapp.methods.projectCount().call() 
          console.log("Projects Length:", projectCount)


            this.setState({ProjectDapp})
            //  this.setState({projectCount})
    
    //       // Load Projects
          for(var j=1; j <= projectCount; j++){
            const project = await ProjectDapp.methods.projects(j).call()
            this.setState({
              projects:[...this.state.projects, project]
            })
          }
        //   // Load Contributors
        //   for(var i=1; i <= contributorsCount; i++){
        //     const contributor = await ProjectDapp.methods.contributions(i).call()
        //     this.setState({
        //         contributors:[...this.state.contributors, contributor]
        //     })
        //   }
          console.log({projects:this.state.projects})
        //   console.log({contributors:this.state.contributors})
      }else {
              window.alert("Marketplace contract is not deployed to the network")
            }

  }

    // constructor
    constructor(props) {
        super(props)
        this.state ={
         account:'',
         ProjectDapp:null,
         projects:[],
         contributors:[],
         loader:false,
        //  message:''
        }
       }

  render() { 
    return (
      <Router>
        <div>
          <header id="header">
            <div className="top-bar">
              <div className="container">
                <div className="row">
                  <div className="col-sm-6 col-xs-12">
                    <div className="top-number">
                      <p>
                        <i className="fa fa-phone-square"></i>{" "}
                        {this.state.account}
                      </p>
                    </div>
                  </div>
                  <div className="col-sm-6 col-xs-12">
                    <div className="social">
                      <ul className="social-share">
                        <li>
                          <a href="https://www.facebook.com/">
                            <i className="fa fa-facebook"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="fa fa-twitter"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="fa fa-linkedin"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="fa fa-dribbble"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="fa fa-skype"></i>
                          </a>
                        </li>
                      </ul>
                      <div className="search">
                        <form role="form">
                          <input
                            type="text"
                            className="search-form"
                            autocomplete="off"
                            placeholder="Search"
                          />
                          <i className="fa fa-search"></i>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* <!--/.container--> */}
            </div>
            {/* <!--/.top-bar--> */}

            <nav className="navbar navbar-inverse" role="banner">
              <div className="container">
                <div className="navbar-header">
                  <button
                    type="button"
                    className="navbar-toggle"
                    data-toggle="collapse"
                    data-target=".navbar-collapse"
                  >
                    <span className="sr-only">Toggle navigation</span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                  </button>
                  <Link className="navbar-brand" to="index.html">
                    <img src="/images/logo.png" alt="logo" />
                  </Link>
                </div>

                <div className="collapse navbar-collapse navbar-right">
                  <ul className="nav navbar-nav">
                    <li className="active">
                      <Link to="/">Home</Link>
                    </li>
                    {/* <li><Link to="/about">About Us</Link></li>
                        <li><a href="#">Services</a></li> */}
                        <li><Link to="/projects">Projects</Link></li>
      
                        <li><Link to="add/">Start a project</Link></li> 
                        {/* <li><a href="#">Blog</a></li>*/}
                    </ul>
                </div>
            </div>
            {/* <!--/.container--> */}
        </nav>
        {/* <!--/nav--> */}

    </header>
    {/* <!--/header--> */}
    <Switch>
        <Route path="/projects">
            <Projects 
            projects = {this.state.projects}
            ProjectDapp={this.state.ProjectDapp}
            account={this.state.account}

            />
            {/* <Seller 
            loading={this.state.loading}
            productDapp={this.state.productDapp}
            message ={this.state.message}
            account={this.state.account}
            /> */}
        </Route>
        <Route path="/add">
        { this.state.loader 
        ?      <div class="cssload-loader-inner">
                    <div class="cssload-cssload-loader-line-wrap-wrap">
                        <div class="cssload-loader-line-wrap"></div>
                    </div>
                    <div class="cssload-cssload-loader-line-wrap-wrap">
                        <div class="cssload-loader-line-wrap"></div>
                    </div>
                    <div class="cssload-cssload-loader-line-wrap-wrap">
                        <div class="cssload-loader-line-wrap"></div>
                    </div>
                    <div class="cssload-cssload-loader-line-wrap-wrap">
                        <div class="cssload-loader-line-wrap"></div>
                    </div>
                    <div class="cssload-cssload-loader-line-wrap-wrap">
                        <div class="cssload-loader-line-wrap"></div>
                    </div>
            	</div>
            :<Form
            ProjectDapp={this.state.ProjectDapp}
            projects = {this.state.projects}
            loader ={this.state.loaders}
            account={this.state.account}
            />}
            {/* <Seller 
            loading={this.state.loading}
            productDapp={this.state.productDapp}
            message ={this.state.message}
            account={this.state.account}
            /> */}
        </Route>
        
        <Route path="/">
            <Home 
            />
            {/* <Seller 
            loading={this.state.loading}
            productDapp={this.state.productDapp}
            message ={this.state.message}
            account={this.state.account}
            /> */}
            </Route>
          </Switch>

          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
