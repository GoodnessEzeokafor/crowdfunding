pragma solidity ^0.5.0;
pragma experimental ABIEncoderV2;
import "../../node_modules/@openzeppelin/contracts/math/SafeMath.sol";




contract Project{
    using SafeMath for uint256;
    enum State{
        Fundraising,
        Expired,
        Successful
    }

    uint public projectCount;
    uint public contributionCount;
    string public dapp_name = "FundProj";
    State public state = State.Fundraising;
    mapping (address => uint) public contributions;
    // Modifier:
    //       Modifier that checks the current state 
    // */
    modifier inState(State _state){
        require(state == _state);
        _;
    }
   
    // Modifier:
    //      Modifier to check if the function caller is the project creator
    
    mapping(uint => Project) public projects;
    // Project[] public projects;

    struct Project{
        uint id;
        address payable creator;
        string name;
        string description;
        string slide;
        string slide2;
        string video_url;
        uint goal;
        uint created;
        uint deadline;
        uint currentBalance;
        uint completedAt;
    }
    event ProjectCreated(
        uint id,
        address payable creator,
        string name,
        string description,
        string slide,
        string video_url,
        uint goal,
        uint created,
        uint deadline,
        uint currentBalance
    );
    event CreatorPaid(address recipient);
    event FundReceived(address contributor, uint amount, uint currentTotal);
    event ProjectCompletedAt(string name, uint currentBalance, uint completedAt);

    function createProject(
        string memory _name,
        string memory _description,
        string memory _slide,
        string memory _slide2,
        string memory _video_url,
        uint _goal,
        uint _deadline
    )public{
    projectCount++;
    Project memory newProject = Project({
        id:projectCount,
        creator:msg.sender,
        name:_name,
        description:_description,
        slide:_slide,
        slide2:_slide2,
        video_url : _video_url,
        goal:_goal,
        created:now,
        deadline:_deadline,
        currentBalance:0,
        completedAt:0
    });
    // newProject.currentBalance = 0;
    projects[projectCount] = newProject;
    // projects.push(newProject); // adds to the array
    emit ProjectCreated(
        projectCount,
        msg.sender,
          _name,
         _description,
         _slide,
        _video_url,
        _goal,
        now,
        _deadline,
        0
    );
    }



     
     function contribute(uint _id) external payable{
         contributionCount++;
        Project memory _project = projects[_id];
         require(msg.sender != _project.creator);
         require(msg.value > 0);
         contributions[msg.sender] = contributions[msg.sender].add(msg.value);
        _project.currentBalance =  _project.currentBalance + msg.value;

        //  currentBalance = currentBalance.add(msg.value); //updates the totalbalance
        projects[_id] = _project; // updates the 
         emit FundReceived(msg.sender, msg.value, _project.currentBalance);
         checkIfFundingCompleteOrExpired(_id);
     }
     function checkIfFundingCompleteOrExpired(uint _id) public{
        Project memory _project = projects[_id];
          if(_project.currentBalance >= _project.goal){
            //   state = State.Successful;
              payOut(_id);
          } else if(now > _project.deadline){
                // state = State.Expired;   
                _project.completedAt = now;
                projects[_id] = _project;
          }
          _project.completedAt = now;
          emit ProjectCompletedAt(
                _project.name,
                _project.currentBalance,
                _project.completedAt
         );
      }
              

        function payOut(uint _id) internal  returns (bool){
            Project memory _project = projects[_id];
            uint256 totalRaised = _project.currentBalance;
            _project.currentBalance = 0;
            if(_project.creator.send(totalRaised)){
                emit CreatorPaid(_project.creator);
            }else{
                _project.currentBalance = totalRaised;
                // state = State.Successful;
            }
            return false;
        }

  function get_single_project(uint _id)public view returns(
                                  uint id,
                                  string memory,
                                  string memory,
                                  uint,
                                  uint,
                                  uint,
                                 address,
                                 string memory,
                                 string memory,
                                 string memory ,
                                 uint goal         
                                  ){
     Project memory p = projects[_id];
        return (
            p.id,
            p.name,
            p.description,
            p.deadline,
            p.currentBalance,
            p.completedAt,
            p.creator,
            p.slide,
            p.slide2,
            p.video_url,
            p.goal
        );
  }
}
// https://kovan.etherscan.io/address/0x9273341b5d613fc14c9c00c8b2dfb1568fb57e37